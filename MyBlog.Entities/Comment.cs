﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyBlog.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int ArticleId { get; set; }
        [DataType(DataType.MultilineText)]
        public string CommentDetail { get; set; }
        public DateTime CommentDate { get; set; }
        public bool IsBlocked { get; set; }
        
    }
}