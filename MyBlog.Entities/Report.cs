﻿using System;

namespace MyBlog.Entities
{
    public class Report
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int CommentId { get; set; }
        public string ReportDetail { get; set; }
        public DateTime ReportDate { get; set; }
    }
}