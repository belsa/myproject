﻿using System;
using System.ComponentModel.DataAnnotations;


namespace MyBlog.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public int RankNum { get; set; }
        public int RankSum { get; set; }
        public string Image { get; set; }
        [Display(Name = "Article Detail")]
        [DataType(DataType.MultilineText)]
        public string Detail { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreateDate { get; set; }
    }
}