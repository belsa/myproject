﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Entities;

namespace MyBlog.Data.Context
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>,IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("ApplicationDbContext", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }

        public virtual IDbSet<Article> Article { get; set; }
        public virtual IDbSet<Comment> Comment { get; set; }
        public virtual IDbSet<Report> Report { get; set; }
        public virtual IDbSet<UserRanks> UserRanks { get; set; }
        public override IDbSet<ApplicationUser> Users { get; set; }
        public override IDbSet<IdentityRole> Roles { get; set; }
        public virtual  IDbSet<IdentityUserRole> UserRoles { get; set; }
    }

    public interface IApplicationDbContext
    {

        IDbSet<Article> Article { get; set; }
        IDbSet<Comment> Comment { get; set; }
        IDbSet<Report> Report { get; set; }
        IDbSet<UserRanks> UserRanks { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }
        IDbSet<IdentityRole> Roles { get; set; }
        IDbSet<IdentityUserRole> UserRoles { get; set; }

        int SaveChanges();
    }
}