﻿using System.Data.Entity;
using MyBlog.Entities.Models;

namespace MyBlog.Data.Context
{
    public class BlogDbContext : DbContext {
        public BlogDbContext() : base("ApplicationDbContext") {

        }

        public DbSet<Article> Article { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Report> Report { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder) { } }

}
