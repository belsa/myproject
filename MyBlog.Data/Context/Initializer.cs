﻿using System;
using System.Collections.Generic;
using MyBlog.Entities.Models;

namespace MyBlog.Data.Context
{
    public class Initializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BlogDbContext>
    {
        protected override void Seed(BlogDbContext context)
        {

            //users.ForEach(s => context.Users.Add(s));
            //context.SaveChanges();

            var articles = new List<Article>
            {
            new Article{Id=1,UserId="",Detail="NweArticlesDetail",Title="NewArticleTitle",RankNum=1,RankSum=5,Image=null,CreateDate=DateTime.Now,UserName=""}
            };
            articles.ForEach(s => context.Article.Add(s));
            context.SaveChanges();

        }
    }
}
