﻿using System.Collections.Generic;
using MyBlog.Entities;
using MyBlog.Data.Repository;

namespace MyBlog.Data.Service
{
    public class CommentService : IServiceComment
    {
        private readonly IRepositoryComment _iCommentRepository;
        public CommentService(IRepositoryComment commentRepository)
        {
            _iCommentRepository = commentRepository;
        }
        public int Create(Comment comment)
        {
            return _iCommentRepository.Create(comment);
        }
        public void Edit(Comment comment)
        {
            _iCommentRepository.Edit(comment);
        }
        public void Delete(Comment comment)
        {
            _iCommentRepository.Delete(comment);
        }
        public IEnumerable<Comment> FindAll(int id)
        {
            return _iCommentRepository.FindAll(id);
        }
        public IEnumerable<Comment> FindAll()
        {
            return _iCommentRepository.FindAll();
        }
        public Comment FindById(int id)
        {
            return _iCommentRepository.FindById(id);
        }
        public void SendMail()
        {
            //implement here..
        }
        public void ManageProfiles()
        {
            //implement here..
        }
    }
}
