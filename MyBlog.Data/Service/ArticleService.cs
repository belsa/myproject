﻿using System.Collections.Generic;
using MyBlog.Entities;
using MyBlog.Data.Repository;

namespace MyBlog.Data.Service
{
    public class ArticleService : IServiceArticle
    {
        private readonly IRepositoryArticle _iArticleRepository;
        public ArticleService(IRepositoryArticle articleRepository)
        {
            _iArticleRepository = articleRepository;
        }
        public int Create(Article article)
        {
            return _iArticleRepository.Create(article);
        }
        public void Edit(Article article)
        {
            _iArticleRepository.Edit(article);
        }
        public void Delete(Article article)
        {
            _iArticleRepository.Delete(article);
        }
        public IEnumerable<Article> FindAll()
        {
            return _iArticleRepository.FindAll();
        }
        public IEnumerable<Article> FindAllOrderByDate()
        {
            return _iArticleRepository.FindAllOrderByDate();
        }
        public IEnumerable<Article> FindAllBySearchQuery(string query)
        {
            return _iArticleRepository.FindAllBySearchQuery(query);
        }

        public Article FindById(int id)
        {
            return _iArticleRepository.FindById(id);
        }
        public void RankArticle(int articleId,string userId,int rank)
        {
            _iArticleRepository.RankArticle(articleId, userId, rank);
        }
        //public ArticleCommentsViewModel FindArticleComments(int id)
        //{
        //    return _iArticleRepository.FindArticleComments(id);
        //}
        public void SendMail()
        {
            //implement here..
        }
        public void ManageProfiles()
        {
            //implement here..
        }

    }
}
