﻿using System.Collections.Generic;

namespace MyBlog.Data.Service
{
    public interface IService<T>
    {
        int Create(T t);
        void Edit(T t);
        void Delete(T t);
        IEnumerable<T> FindAll();
        T FindById(int id);
        void SendMail();
        void ManageProfiles();
    }
}
