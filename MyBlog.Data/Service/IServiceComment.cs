﻿using System.Collections.Generic;
using MyBlog.Entities;


namespace MyBlog.Data.Service
{
    public interface IServiceComment : IService<Comment>
    {
        IEnumerable<Comment> FindAll(int id);
    }
}
