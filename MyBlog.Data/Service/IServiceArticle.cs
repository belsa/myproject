﻿using System.Collections.Generic;
using MyBlog.Entities;


namespace MyBlog.Data.Service
{
    public interface IServiceArticle:IService<Article>
    {
        IEnumerable<Article> FindAllOrderByDate();
        IEnumerable<Article> FindAllBySearchQuery(string query);
        void RankArticle(int articleId,string userId, int rank);
    }
}
