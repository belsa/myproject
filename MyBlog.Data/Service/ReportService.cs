﻿using System.Collections.Generic;
using MyBlog.Entities;
using MyBlog.Data.Repository;

namespace MyBlog.Data.Service
{
    public class ReportService : IServiceReport
    {
        private readonly IRepositoryReport _iReportRepository;
        public ReportService(IRepositoryReport reportRepository)
        {
            _iReportRepository = reportRepository;
        }
        public int Create(Report report)
        {
            return _iReportRepository.Create(report);
        }
        public void Edit(Report report)
        {
            _iReportRepository.Edit(report);
        }
        public void Delete(Report report)
        {
            _iReportRepository.Delete(report);
        }
        public IEnumerable<Report> FindAll()
        {
            return _iReportRepository.FindAll();
        }

        public Report FindById(int id)
        {
            return _iReportRepository.FindById(id);
        }

        public void SendMail()
        {
            //implement here..
        }
        public void ManageProfiles()
        {
            //implement here..
        }

    }
}
