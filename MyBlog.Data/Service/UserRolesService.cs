﻿using System.Collections.Generic;
using MyBlog.Entities;
using MyBlog.Data.Repository;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace MyBlog.Data.Service
{
    public class UserRoleService : IServiceUserRoles
    {
        private readonly IRepositoryUserRoles _iUserRolesRepository;
        public UserRoleService(IRepositoryUserRoles userRolesRepository)
        {
            _iUserRolesRepository = userRolesRepository;
        }
        public void Create(string roleName)
        {

            _iUserRolesRepository.Create(roleName);
        }

        public void Edit(IdentityRole role)
        {
            _iUserRolesRepository.Edit(role);

        }

        public void Delete(string roleId)
        {

            _iUserRolesRepository.Delete(roleId);
        }

        public List<SelectListItem> FindAll()
        {
            return _iUserRolesRepository.FindAll();

        }

        public List<SelectListItem> FindAllUsers()
        {

            return _iUserRolesRepository.FindAllUsers();
        }

        public IdentityRole FindById(string roleId)
        {

            return _iUserRolesRepository.FindById(roleId);
        }

        public void RoleAddToUser(string userId, string roleId)
        {
            _iUserRolesRepository.RoleAddToUser(userId, roleId);

        }

        public Task<IList<string>> GetRoles(string userId)
        {
            return _iUserRolesRepository.GetRoles(userId);

        }
        public void DeleteRoleForUser(string userId, string roleId)
        {
            _iUserRolesRepository.DeleteRoleForUserAsync(userId, roleId);

        }

        public Task<IdentityUser> FindUser(string userName, string password)
        {

            return _iUserRolesRepository.FindUser(userName, password);
        }

    }
}
