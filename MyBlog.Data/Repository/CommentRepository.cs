﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MyBlog.Data.Context;
using MyBlog.Entities;
namespace MyBlog.Data.Repository
{
    public class CommentRepository : IRepositoryComment
    {
        private readonly IApplicationDbContext _dbContext;
        public CommentRepository(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;

        }

        public int Create(Comment comment)
        {
            if (comment != null)
            {
                _dbContext.Comment.Add(comment);
                _dbContext.SaveChanges();
                return 1;
            }
            return 0;
        }
        public void Edit(Comment comment)
        {
            if (comment != null)
            {
                //_dbContext.Entry(comment).State = EntityState.Modified;
                var update = _dbContext.Comment.Where(x => x.Id == comment.Id).Single();
                update.CommentDate = comment.CommentDate;
                update.CommentDetail = comment.CommentDetail;
                update.IsBlocked = comment.IsBlocked;
                update.UserId = comment.UserId;
                update.UserName = comment.UserName;
                _dbContext.SaveChanges();
            }

        }
        public void Delete(Comment comment)
        {
            if (comment != null)
            {
                _dbContext.Comment.Remove(comment);
                _dbContext.SaveChanges();
            }

        }
        public IEnumerable<Comment> FindAll()
        {
            return _dbContext.Comment.ToList();
        }
        public IEnumerable<Comment> FindAll(int id)
        {
            return _dbContext.Comment.Where(x=> x.ArticleId == id &&
            x.IsBlocked==false).ToList();
        }
        public Comment FindById(int id)
        {
            return _dbContext.Comment.Where(x => x.Id == id).Single();
        }

    }
}
