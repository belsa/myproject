﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Data.Context;
using MyBlog.Entities;

namespace MyBlog.Data.Repository
{
    public class ArticleRepository : IRepositoryArticle
    {
        private readonly IApplicationDbContext _dbContext;

        public ArticleRepository(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int Create(Article article)
        {
            if (article != null)
            {
                _dbContext.Article.Add(article);
                _dbContext.SaveChanges();
                return 1;
            }
            return 0;
        }
        public void Edit(Article article)
        {
            if (article != null)
            {
                // _dbContext.Entry(article).State = EntityState.Modified;

                var update = _dbContext.Article.Where(x => x.Id == article.Id).Single();
                update.Title = article.Title;
                update.CreateDate = article.CreateDate;
                update.Detail = article.Detail;
                update.Image = article.Image;
                update.RankNum = article.RankNum;
                update.RankSum = article.RankSum;
                _dbContext.SaveChanges();
            }

        }
        public void Delete(Article article)
        {
            if (article != null)
            {
                _dbContext.Article.Remove(article);
                _dbContext.SaveChanges();
            }

        }
        public IEnumerable<Article> FindAll()
        {
            return _dbContext.Article.ToList();

        }
        public IEnumerable<Article> FindAllOrderByDate()
        {

            var list = _dbContext.Article.OrderByDescending(x => x.CreateDate).Take(5);
            return list.ToList();

        }
        public IEnumerable<Article> FindAllBySearchQuery(string query)
        {
            var articles = _dbContext.Article.Where(x => x.Title.Contains(query)
            || x.Detail.Contains(query));
            return articles.ToList();
        }

        public Article FindById(int id)
        {
            return _dbContext.Article.Where(x => x.Id == id).Single();
        }
        public void RankArticle(int articleId,string userId, int rank)
        {
           
            var userRank = _dbContext.UserRanks.Where(x => x.UserId == userId && x.ArticleId== articleId).FirstOrDefault();

            if (userRank == null)
            {
                UserRanks newRank = new UserRanks();
                newRank.ArticleId = articleId;
                newRank.UserId = userId;
                newRank.Rank = rank;
                _dbContext.UserRanks.Add(newRank);
                _dbContext.SaveChanges();
            }
            else
            {
                userRank.Rank = rank;
                _dbContext.SaveChanges();
            }

            var sumRanks = _dbContext.UserRanks.Where(x => x.ArticleId == articleId);

            var sum = sumRanks.Sum(x => x.Rank);
            var count = sumRanks.Count();

            var article = _dbContext.Article.Where(x => x.Id == articleId).Single();
            article.RankNum = count;
            article.RankSum = sum;
            _dbContext.SaveChanges();
        }
       
        
    }
}
