﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MyBlog.Data.Context;
using MyBlog.Entities;

namespace MyBlog.Data.Repository
{
    public class ReportRepository : IRepositoryReport
    {
        private readonly IApplicationDbContext _dbContext;
        public ReportRepository(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int Create(Report report)
        {
            if (report != null)
            {
                _dbContext.Report.Add(report);
                _dbContext.SaveChanges();
                return 1;
            }
            return 0;
        }
        public void Edit(Report report)
        {
            if (report != null)
            {

                //_dbContext.Entry(report).State = EntityState.Modified;
                _dbContext.SaveChanges();
            }

        }
        public void Delete(Report report)
        {
            if (report != null)
            {
                _dbContext.Report.Remove(report);
                _dbContext.SaveChanges();
            }

        }
        public IEnumerable<Report> FindAll()
        {
            return _dbContext.Report.ToList();
        }
        public Report FindById(int id)
        {
            return _dbContext.Report.Where(x => x.Id == id).Single();
        }

    }
}
