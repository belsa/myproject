﻿using System.Collections.Generic;
using MyBlog.Entities;
namespace MyBlog.Data.Repository
{
    public interface IRepositoryComment: IRepository<Comment>
    {
        IEnumerable<Comment> FindAll(int id);

    }
}
