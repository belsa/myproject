﻿using System.Collections.Generic;

namespace MyBlog.Data.Repository
{
    public interface IRepository<T>
    {
        int Create(T t);
        void Edit(T t);
        void Delete(T t);
        IEnumerable<T> FindAll();
        T FindById(int id);

    }
}
