﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Data.Context;

namespace MyBlog.Data.Repository
{
    public class UserRolesRepository : IRepositoryUserRoles
    {
        private readonly ApplicationDbContext _dbContext;

        public UserRolesRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            
        }

        public void Create(string roleName)
        {

            _dbContext.Roles.Add(new IdentityRole()
            {
                Name = roleName
               
        });
            _dbContext.SaveChanges();
        }
        public void Edit(IdentityRole role)
        {
            var thisRole = _dbContext.Roles.Where(x => x.Id == role.Id).Single();
            thisRole.Name = role.Name;
            _dbContext.SaveChanges();
           
        }
        public void Delete(string roleId)
        {

            var thisRole = _dbContext.Roles.Where(x => x.Id == roleId).Single();
           // _dbContext.Roles.Find(roleId);
            
            _dbContext.Roles.Remove(thisRole);
            _dbContext.SaveChanges();

        }
        public List<SelectListItem> FindAll()
        {
            var rolelist =  _dbContext.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
            new SelectListItem { Value = rr.Id.ToString(), Text = rr.Name }).ToList();

            return rolelist;

        }
        public List<SelectListItem> FindAllUsers()
        {
            var userlist = _dbContext.Users.OrderBy(u => u.UserName).ToList().Select(uu =>
            new SelectListItem { Value = uu.Id.ToString(), Text = uu.UserName }).ToList();

            return userlist;
        }

        public IdentityRole FindById(string roleId)
        {
            return _dbContext.Roles.Where(x => x.Id == roleId).Single();
        }
        public void RoleAddToUser(string userId, string roleId)
        {
            ApplicationUser user = _dbContext.Users.Where(x => x.Id == userId).Single();

            var role = FindById(roleId);

            var userStore = new UserStore<ApplicationUser>(_dbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);
            userManager.AddToRoleAsync(user.Id, role.Name);
        }
        public async Task<IList<string>> GetRoles(string userId)
        {
            ApplicationUser user = _dbContext.Users.Where(x => x.Id == userId).Single();

            var userStore = new UserStore<ApplicationUser>(_dbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            return await  userManager.GetRolesAsync(user.Id);
        }
        public async Task DeleteRoleForUserAsync(string userId, string roleId)
        {
            ApplicationUser user = _dbContext.Users.Where(x => x.Id == userId).Single();
            var role = FindById(roleId);
            var userStore = new UserStore<ApplicationUser>(_dbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (await userManager.IsInRoleAsync(user.Id, role.Name))
            //if (userManager.IsInRoleAsync(user.Id, role.Name))
            {
              await  userManager.RemoveFromRolesAsync(user.Id, role.Name);
            }
        }
        public ApplicationUser GetUserInfo(string UserId)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_dbContext));
            var user = manager.FindById(UserId);
            ApplicationUser EmpUser = user;
            return EmpUser;

        }
        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var userStore = new UserStore<ApplicationUser>(_dbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);
            IdentityUser user = await userManager.FindAsync(userName, password);

            return user;
        }



    }
}
