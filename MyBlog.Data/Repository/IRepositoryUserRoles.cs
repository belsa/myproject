﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MyBlog.Data.Repository
{
    public interface IRepositoryUserRoles
        
    {
        void Create(string roleName);

        void Edit(IdentityRole role);

        void Delete(string roleId);

        List<SelectListItem> FindAll();

        List<SelectListItem> FindAllUsers();

        IdentityRole FindById(string roleId);

        void RoleAddToUser(string userId, string roleId);

        Task<IList<string>> GetRoles(string userId);
        Task DeleteRoleForUserAsync(string userId, string roleId);
        Task<IdentityUser> FindUser(string userName, string password);



    }
}
