﻿using System.Collections.Generic;
using MyBlog.Entities;

namespace MyBlog.Data.Repository
{
    public interface IRepositoryArticle : IRepository<Article>
    {
        IEnumerable<Article> FindAllOrderByDate();
        IEnumerable<Article> FindAllBySearchQuery(string query);
        void RankArticle(int articleId, string userId, int rank);
    }
}
