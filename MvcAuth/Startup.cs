﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using MyBlog.Data.Context;
using MyBlog.Data.Service;
using Owin;
using System.Web.Http;



[assembly: OwinStartupAttribute(typeof(MvcAuth.Startup))]
namespace MvcAuth
{
    public partial class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddTransient<Article>();

            // services.AddTransient<IRepository<ArticleModel>,ArticleRepository>();
            // services.AddTransient < IService<ArticleModel>,ArticleService>();
            //   services.AddIdentity<ApplicationUser, IdentityRole>()
            //.AddEntityFrameworkStores<ApplicationDbContext>()
            //.AddDefaultTokenProviders();
            // Add application services.
           // services.AddTransient<IEmailSender, EmailSender>();
            services.AddIdentity<ApplicationUser, IdentityRole>().AddDefaultTokenProviders();


        }
        public void Configuration(IAppBuilder app)
        {
            //var config = new HttpConfiguration();
            //Register(app);
            ConfigureAuth(app);
            ConfigureReg(app);
           
           

        }
    }
}
