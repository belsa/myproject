﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MyBlog.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public int Rank { get; set; }
        public string Image { get; set; }
        [Display(Name ="Article Detail")]
        [DataType(DataType.MultilineText)]
        public string Detail { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreateDate { get; set; }
    }


}