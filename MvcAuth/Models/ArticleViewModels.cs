﻿using MyBlog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcAuth.Models
{
    public class ArticleViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public int RankNum { get; set; }
        public int RankSum { get; set; }
        public string Image { get; set; }
        [Display(Name = "Article Detail")]
        [DataType(DataType.MultilineText)]
        public string Detail { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreateDate { get; set; }

    }
    public class ArticleCommentsViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public int RankNum { get; set; }
        public int RankSum { get; set; }
        public string Image { get; set; }
        [Display(Name = "Article Detail")]
        [DataType(DataType.MultilineText)]
        public string Detail { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreateDate { get; set; }
        public string NewComment { get; set; }
        public List<CommentViewModel> Comments { get; set; }
        [DisplayFormat(DataFormatString = "{0:#.#}")]
        public double Rank { get; set; }

    }
    public class ArticleViewModelMapper
    {
        public Article MapFromModel(ArticleViewModel articleModel)
        {
            return new Article
            {
                Id = articleModel.Id,
                UserId = articleModel.UserId,
                UserName = articleModel.UserName,
                Title = articleModel.Title,
                RankNum = articleModel.RankNum,
                RankSum = articleModel.RankSum,
                Image = articleModel.Image,
                Detail = articleModel.Detail,
                CreateDate = articleModel.CreateDate
            };
        }
        public ArticleViewModel MapToModel(Article article)
        {
            return new ArticleViewModel
            {
                Id = article.Id,
                UserId = article.UserId,
                UserName = article.UserName,
                Title = article.Title,
                RankNum = article.RankNum,
                RankSum = article.RankSum,
                Image = article.Image,
                Detail = article.Detail,
                CreateDate = article.CreateDate
            };
        }


    }

    


}