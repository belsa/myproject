﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyBlog.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int ArticleId { get; set; }
        [DataType(DataType.MultilineText)]
        public string CommentDetail { get; set; }
        public DateTime CommentDate { get; set; }
        public bool IsBlocked { get; set; }
    }


}