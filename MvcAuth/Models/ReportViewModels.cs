﻿using System;
using System.ComponentModel.DataAnnotations;
using MyBlog.Entities;

namespace MvcAuth.Models
{
    public class ReportViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int CommentId { get; set; }
        [DataType(DataType.MultilineText)]
        public string ReportDetail { get; set; }
        public DateTime ReportDate { get; set; }
    }
    public class ReportViewModelMapper
    {
        public Report MapFromModel(ReportViewModel reportModel)
        {
            return new Report
            {
                Id = reportModel.Id,
                CommentId = reportModel.CommentId,
                UserId = reportModel.UserId,
                UserName = reportModel.UserName,
                ReportDate = reportModel.ReportDate,
                ReportDetail = reportModel.ReportDetail,
            };
        }
        public ReportViewModel MapToModel(Report report)
        {
            return new ReportViewModel
            {
                Id = report.Id,
                CommentId = report.CommentId,
                UserId = report.UserId,
                UserName = report.UserName,
                ReportDate = report.ReportDate,
                ReportDetail = report.ReportDetail,
            };
        }


    }

}