﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAuth.Models.Api
{
    public class ArticleEditModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int RankNum { get; set; }
        public int RankSum { get; set; }
        public string Image { get; set; }
        public string Detail { get; set; }
        public string File { get; set; }
    }
}