﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAuth.Models.Api
{
    public class CommentCreateModel
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string CommentDetail { get; set; }
        public DateTime CommentDate { get; set; }
        public bool IsBlocked { get; set; }
    }
}