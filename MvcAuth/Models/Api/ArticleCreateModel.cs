﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAuth.Models.Api
{
    public class ArticleCreateModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }   
        public string Image { get; set; }
        public string Detail { get; set; }
        public DateTime CreateDate { get; set; }
       public string File { get; set; }
    }
}