﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAuth.Models.Api
{
    public class ArticleRankModel
    {
        public int ArticleId { get; set; }
        public string UserId { get; set; }
        public int Rank { get; set; }
    }

}