﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAuth.Models.Api
{
    public class ArticleDetailModel
    {
            public int Id { get; set; }
            public string UserName { get; set; }
            public string Title { get; set; }
            public double Rank { get; set; }
            public string Detail { get; set; }
            public DateTime CreateDate { get; set; }
            public List<CommentModel> Comments { get; set; }
    }
}