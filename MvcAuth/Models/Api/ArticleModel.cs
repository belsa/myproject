﻿namespace MvcAuth.Models.Api
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
    }
}