﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBlog.Models;
using MyBlog.ViewModels;


namespace MvcAuth.Models
{
    public class ArticleService : IService<ArticleModel>
    {
        private readonly IRepository<ArticleModel> _iArticleRepository;
        public ArticleService(IRepository<ArticleModel> articleRepository)
        {
            _iArticleRepository = articleRepository;
        }
        public int Create(ArticleModel article)
        {
            return _iArticleRepository.Create(article);
        }
        public void Edit(ArticleModel article)
        {
             _iArticleRepository.Edit(article);
        }
        public void Delete(ArticleModel article)
        {
             _iArticleRepository.Delete(article);
        }
        public IEnumerable<ArticleModel> FindAll()
        {
            return _iArticleRepository.FindAll();
        }
        public IEnumerable<ArticleModel> FindAllOrderByDate()
        {
            return _iArticleRepository.FindAllOrderByDate();
        }
        public IEnumerable<ArticleModel> FindAllBySearchQuery(string query)
        {
            return _iArticleRepository.FindAllBySearchQuery(query);
        }

        public ArticleModel FindById(int id)
        {
            return _iArticleRepository.FindById(id);
        }
        public ArticleCommentsViewModel FindArticleComments(int id)
        {
            return _iArticleRepository.FindArticleComments(id);
        }
        public void SendMail()
        {
            //implement here..
        }
        public  void CalculateTotalProfit()
        {
            //implement here..
        }
    }

}