﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MyBlog.DAL;
using MyBlog.Models;
using MyBlog.ViewModels;

namespace MvcAuth.Models
{
    public class ArticleRepository : IRepository<ArticleModel>
    {
         BlogContext dbContext = new BlogContext();

        public int Create(ArticleModel article)
        {
            if (article != null)
            {
                dbContext.Articles.Add(article);
                dbContext.SaveChanges();
                return 1;
            }
            return 0;
        }
        public void Edit(ArticleModel article)
        {
            if (article != null)
            {
                dbContext.Entry(article).State = EntityState.Modified;
                dbContext.SaveChanges();
            }

        }
        public void Delete(ArticleModel article)
        {
            if (article != null)
            {
                dbContext.Articles.Remove(article);
                dbContext.SaveChanges();
            }

        }
        public IEnumerable<ArticleModel> FindAll()
        {
            return dbContext.Articles.ToList();
        }
        public IEnumerable<ArticleModel> FindAllOrderByDate()
        {
            //var list = (from t in dbContext.Articles
            //            orderby t.CreateDate descending
            //            select t).Take(5);

            var list = dbContext.Articles.OrderByDescending(x => x.CreateDate).Take(5);
            return list.ToList();

        }
        public IEnumerable<ArticleModel> FindAllBySearchQuery(string query)
        {
            var articles = dbContext.Articles.Where(x => x.Title.Contains(query)
            || x.Detail.Contains(query));
            return articles.ToList();
        }

        public List<CommentModel> FindAllComments(int id)
        {
            var comments = from m in dbContext.Comments
                           where m.ArticleId == id && m.IsBlocked == false
                           select m;
            return comments.ToList();
        }
        public ArticleModel FindById(int id)
        {
            return dbContext.Articles.Find(id);
        }
        public ArticleCommentsViewModel FindArticleComments(int id)
        {
            var articles = FindById(id);
            var comments= dbContext.Comments.Find(id);

            ArticleCommentsViewModel viewModel = new ArticleCommentsViewModel();
            viewModel.Article = dbContext.Articles.Find(id);
            viewModel.UserName = "";
            viewModel.Comments = FindAllComments(id);
            return viewModel;
        }
    }
}