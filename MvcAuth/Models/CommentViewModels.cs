﻿using System;
using System.ComponentModel.DataAnnotations;
using MyBlog.Entities;

namespace MvcAuth.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        [DataType(DataType.MultilineText)]
        public string CommentDetail { get; set; }
        public DateTime CommentDate { get; set; }
        public bool IsBlocked { get; set; }
    }
   
    public class CommentViewModelMapper
    {
        public Comment MapFromModel(CommentViewModel commentModel)
        {
            return new Comment
            {
                Id = commentModel.Id,
                ArticleId=commentModel.ArticleId,
                UserId = commentModel.UserId,
                UserName = commentModel.UserName,
                CommentDate = commentModel.CommentDate,
                CommentDetail = commentModel.CommentDetail,
                IsBlocked= commentModel.IsBlocked
            };
        }
        public CommentViewModel MapToModel(Comment comment)
        {
            return new CommentViewModel
            {
                Id = comment.Id,
                ArticleId = comment.ArticleId,
                UserId = comment.UserId,
                UserName = comment.UserName,
                CommentDate = comment.CommentDate,
                CommentDetail = comment.CommentDetail,
                IsBlocked = comment.IsBlocked
            };
        }


    }

}