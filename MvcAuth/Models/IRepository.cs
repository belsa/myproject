﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBlog.ViewModels;

namespace MvcAuth.Models
{
    public interface IRepository<T>
    {
        int Create(T t);
        void Edit(T t);
        void Delete(T t);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAllOrderByDate();
        IEnumerable<T> FindAllBySearchQuery(string query);
        T FindById(int id);
        ArticleCommentsViewModel FindArticleComments(int id);

    }

}
