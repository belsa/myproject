﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MvcAuth.Models;
using MyBlog.Data.Service;
using System.Collections.Generic;
using AutoMapper;
using MyBlog.Entities;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace MvcAuth.Controllers
{

    public class ArticlesController : Controller
    {
        private readonly IServiceArticle _articleService = null;
        private readonly IServiceComment _commentService = null;

        public ArticlesController(IServiceArticle articleService,
            IServiceComment commentService)
        {
            _articleService = articleService;
            _commentService = commentService;
        }

        public ActionResult Index()
        {
            var articles = _articleService.FindAll().ToList();
            List<ArticleViewModel> list = new List<ArticleViewModel>();
            foreach (var item in articles)
            {
                list.Add(Mapper.Map<Article,ArticleViewModel>(item));
            }
            return View(list);
        }


        public ActionResult Details(int id)
        {
            var article = _articleService.FindById(id);
            var comments = _commentService.FindAll(id);

            ArticleCommentsViewModel model = new ArticleCommentsViewModel();
            model.Title = article.Title;
            model.Id = article.Id;
            model.UserName = article.UserName;
            model.UserId = article.UserId;
            model.Image = article.Image;
            model.CreateDate = article.CreateDate;
            model.Detail = article.Detail;
            model.RankNum = article.RankNum;
            model.RankSum = article.RankSum;
            if (article.RankNum > 0)
                model.Rank = Convert.ToDouble(Convert.ToDouble(article.RankSum) / Convert.ToDouble(article.RankNum));

            List<CommentViewModel> list = new List<CommentViewModel>();
           
            foreach (var item in comments)
            {
                list.Add(Mapper.Map<Comment,CommentViewModel>(item));
            }
            model.Comments = list;

            return View(model);
        }

        // GET: ArticlesModels/Create
        [Authorize(Roles = "Administrator,User")]
        public ActionResult Create()
        {
            return View();
        }
        [ChildActionOnly]
        public ActionResult CreateCommentView(int Id)
        {
            //   articleID = Id;
            CommentViewModel model = new CommentViewModel();
            model.ArticleId = Id;
            return PartialView("CreateCommentView", model);

        }

        // POST: ArticlesModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ArticleViewModel articlesViewModel, HttpPostedFileBase upload)
        {

            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(upload.FileName);
                    upload.SaveAs(Path.Combine(Server.MapPath("~/images"), fileName));
                    articlesViewModel.Image = fileName;
                }
                articlesViewModel.UserId = User.Identity.GetUserId();
                articlesViewModel.UserName = User.Identity.GetUserName();
                articlesViewModel.CreateDate = DateTime.Now;

                var article = Mapper.Map<ArticleViewModel, Article>(articlesViewModel);

                _articleService.Create(article);

                return RedirectToAction("Index");
            }

            return View(articlesViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,User")]
        public ActionResult CreateComment(CommentViewModel commentsModel)
        {
            commentsModel.CommentDate = DateTime.Now;
            commentsModel.IsBlocked = false;
            commentsModel.UserId = User.Identity.GetUserId();
            commentsModel.UserName = User.Identity.GetUserName();
           
            _commentService.Create(Mapper.Map<CommentViewModel, Comment>(commentsModel));

            return RedirectToAction("Details", "Articles", new { Id = commentsModel.ArticleId });
        }

        // GET: ArticlesModels/Edit/5
        [Authorize(Roles = "Administrator,User")]
        public ActionResult Edit(int id)
        {

            var article = _articleService.FindById(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            var articleViewMOdel = Mapper.Map<Article, ArticleViewModel>(article);
            return View(articleViewMOdel);
        }

        // POST: ArticlesModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ArticleViewModel articlesModel, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(upload.FileName);
                    upload.SaveAs(Path.Combine(Server.MapPath("~/images"), fileName));
                    articlesModel.Image = fileName;
                }

                var article = Mapper.Map<ArticleViewModel, Article>(articlesModel);
                _articleService.Edit(article);
                return RedirectToAction("Index");
            }
            return View(articlesModel);
        }

        // GET: ArticlesModels/Delete/5
        [Authorize(Roles = "Administrator,User")]
        public ActionResult Delete(int id)
        {
            var article = _articleService.FindById(id);
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Article, ArticleViewModel>(article));

        }

        // POST: ArticlesModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var article = _articleService.FindById(id);

            _articleService.Delete(article);

            return RedirectToAction("Index");
        }
        // POST: ArticlesModels/Delete/5
        [HttpPost]
        public ActionResult RankArticle(int rank, int id,string userId)
        {
           // string userId = User.Identity.GetUserId();
            _articleService.RankArticle(id, userId, rank);
            return RedirectToAction("Details", "Articles", new { Id = id });
        }

        public ActionResult RankArticleView(int id)
        {
            //to do rank article
            List<SelectListItem> ranks = new List<SelectListItem>();
            for (int i = 1; i < 6; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                ranks.Add(item);
            }

            ViewBag.RankList = ranks;
            ViewBag.Id = id;
            return PartialView("RankArticleView");

        }
        public async Task<ActionResult> ListApi()
        {
            string Baseurl = "http://localhost:63090/";

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                string result="";
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/data/forall");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    result = JsonConvert.DeserializeObject<string>(EmpResponse);

                }
                //returning the employee list to view  
                ViewBag.Message = result;
                return View();
            }
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
