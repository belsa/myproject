﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MyBlog.Data.Service;
using MvcAuth.Models;
using MyBlog.Entities;


namespace MvcAuth.Controllers
{
    public class CommentsController : Controller
    {
        private readonly IServiceComment _commentService = null;
        public CommentsController(IServiceComment commentService)
        {
            _commentService = commentService;
        }

        // GET: CommentsModels
        public ActionResult Index()
        {
            var comments = _commentService.FindAll();
            List<CommentViewModel> list = new List<CommentViewModel>();
            foreach (var item in comments)
            {
                list.Add(Mapper.Map<Comment, CommentViewModel>(item));
            }
            return View(list);
        }

        // GET: CommentsModels/Details/5
        public ActionResult Details(int id)
        {

            var comment = _commentService.FindById(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Comment,CommentViewModel>(comment));
        }

        // GET: CommentsModels/Edit/5
        [Authorize(Roles = "Administrator,User")]
        public ActionResult Edit(int id)
        {

            var comment = _commentService.FindById(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Comment,CommentViewModel>(comment));
        }

        // POST: CommentsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentViewModel commentViewModel)
        {
            if (ModelState.IsValid)
            {
                _commentService.Edit(Mapper.Map<CommentViewModel,Comment>(commentViewModel));
                return RedirectToAction("Index");
            }
            return View(commentViewModel);
        }

        // GET: CommentsModels/Delete/5
        [Authorize(Roles = "Administrator,User")]
        public ActionResult Delete(int id)
        {
            var comment = _commentService.FindById(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Comment,CommentViewModel>(comment));
        }

        // POST: CommentsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var comment = _commentService.FindById(id);
            _commentService.Delete(comment);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
