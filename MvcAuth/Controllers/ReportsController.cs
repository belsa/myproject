﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using AutoMapper;
using MvcAuth.Models;
using MyBlog.Data.Service;
using MyBlog.Entities;


namespace MvcAuth.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IServiceReport _reportService = null;
        public ReportsController(IServiceReport reportService)
        {
            _reportService = reportService;
        }

        // GET: ReportsModels
        public ActionResult Index()
        {
            var reports = _reportService.FindAll().ToList();
            List<ReportViewModel> list = new List<ReportViewModel>();
            foreach (var item in reports)
            {
                list.Add(Mapper.Map<Report,ReportViewModel>(item));
            }
            return View(list);
        }

        // GET: ReportsModels/Details/5
        public ActionResult Details(int id)
        {
            var report= _reportService.FindById(id);

            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report,ReportViewModel>(report));

        }

        // GET: ReportsModels/Create

        public ActionResult Create(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        // POST: ReportsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ReportViewModel reportViewModel, int id)
        {
            if (ModelState.IsValid)
            {
                reportViewModel.UserId = User.Identity.GetUserId();
                reportViewModel.ReportDate = DateTime.Now;
                reportViewModel.UserName = User.Identity.GetUserName();
                reportViewModel.CommentId = id;
                
                _reportService.Create(Mapper.Map<ReportViewModel,Report>(reportViewModel));
                return RedirectToAction("Index");
            }

            return View(reportViewModel);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
