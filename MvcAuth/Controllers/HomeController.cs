﻿using System.Web.Mvc;
using System.Collections.Generic;
using AutoMapper;
using MyBlog.Data.Service;
using MvcAuth.Models;
using MyBlog.Entities;


namespace MvcAuth.Controllers
{
    public class HomeController : Controller
    {
        //private BlogContext db = new BlogContext();

        private readonly IServiceArticle _homeService = null;
        public HomeController(IServiceArticle homeService)
        { 
            _homeService = homeService;
        }

        public HomeController()
        { }
        public ActionResult Index(string query)
        { 
            List<ArticleViewModel> list = new List<ArticleViewModel>();
           
            if (string.IsNullOrEmpty(query))
            {
                var articles = _homeService.FindAllOrderByDate();
                foreach (var item in articles)
                {
                    list.Add(Mapper.Map<Article,ArticleViewModel>(item));
                }
            }
            else
            {
                var articles = _homeService.FindAllBySearchQuery(query);
                foreach (var item in articles)
                {
                    list.Add(Mapper.Map<Article, ArticleViewModel>(item));
                }

            }
            
            return View(list);
        }
        public ActionResult Details(int Id)
        {
            // return RedirectToAction("actionName");
            // or
            // return RedirectToAction("actionName", "controllerName");
            // or
            return RedirectToAction("Details", "Articles", new {/* routeValues, for example: */ id = Id });
        }

        public ActionResult About()
        {
            //ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //  ViewBag.Message = "Your contact page.";

            return View();
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
