﻿using System.Web.Mvc;
using MyBlog.Data.Service;
 
namespace MvcAuth.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RolesController : Controller
    {
        private readonly IServiceUserRoles _userRolesService = null;

        public RolesController(IServiceUserRoles userRolesService)
        {
            _userRolesService = userRolesService;
        }

        
        public ActionResult Index()
        {
            var rolelist = _userRolesService.FindAll();
            ViewBag.Roles = rolelist;

            var userlist = _userRolesService.FindAllUsers();
            ViewBag.Users = userlist;

            ViewBag.Message = "";

            return View();
        }


        // GET: /Roles/Create
        public ActionResult Create()
        {
            return View();
        }
          
        //
        // POST: /Roles/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {

            try
            {
                _userRolesService.Create(collection["RoleName"]);
                ViewBag.Message = "Role created successfully !";
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Delete(string roleId)
        {
            _userRolesService.Delete(roleId);
            return RedirectToAction("Index");
        }

        //
        // GET: /Roles/Edit/5
        public ActionResult Edit(string roleId)
        {
            var thisRole = _userRolesService.FindById(roleId);
            return View(thisRole);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Microsoft.AspNet.Identity.EntityFramework.IdentityRole role)
        {
          
            try
            {
                _userRolesService.Edit(role);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //  Adding Roles to a user
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RoleAddToUser(string userId, string roleId)
        {
            _userRolesService.RoleAddToUser(userId, roleId);

            ViewBag.Message = "Role created successfully !";

            // Repopulate Dropdown Lists
            var rolelist = _userRolesService.FindAll();
            ViewBag.Roles = rolelist;
            var userlist = _userRolesService.FindAllUsers();
            ViewBag.Users = userlist;

            return View("Index");
        }


        //Getting a List of Roles for a User
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetRoles(string userId)
        {
            if (!string.IsNullOrWhiteSpace(userId))
            {
                ViewBag.RolesForThisUser = _userRolesService.GetRoles(userId);
                // Repopulate Dropdown Lists
                var rolelist = _userRolesService.FindAll();
                ViewBag.Roles = rolelist;
                var userlist = _userRolesService.FindAllUsers();
                ViewBag.Users = userlist;
                ViewBag.Message = "Roles retrieved successfully !";
            }

            return View("Index");
        }

        //Deleting a User from A Role
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRoleForUser(string userId, string roleId)
        {

            
            try
            {
                _userRolesService.DeleteRoleForUser(userId, roleId);
            }
            catch
            {
                ViewBag.Message = "This user doesn't belong to selected role.";
            }

            // Repopulate Dropdown Lists
            var rolelist = _userRolesService.FindAll();
            ViewBag.Roles = rolelist;
            var userlist = _userRolesService.FindAllUsers();
            ViewBag.Users = userlist;

            return View("Index");
        }

    }
}