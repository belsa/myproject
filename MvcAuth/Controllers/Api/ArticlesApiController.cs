﻿using AutoMapper;
using MvcAuth.Models;
using MyBlog.Data.Service;
using MyBlog.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using MvcAuth.Models.Api;
using System.Net.Http;
using System.Net;
using System.Text;

namespace MvcAuth.Controllers.Api
{
    public class ArticlesApiController : ApiController
    {
        private readonly IServiceArticle _articleService = null;
        private readonly IServiceComment _commentService = null;

        public ArticlesApiController(IServiceArticle articleService,
           IServiceComment commentService)
        {
            _articleService = articleService;
            _commentService = commentService;
        }


        //api/articlesapi

        [AllowAnonymous]
        [HttpGet]
        public List<ArticleModel> Get()
        {
            var articles = _articleService.FindAll().ToList();
            List<ArticleModel> list = new List<ArticleModel>();
            foreach (var item in articles)
            {
                list.Add(Mapper.Map<Article, ArticleModel>(item));
            }
            return list;
        }

        //api/articlesapi/5
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(ArticleDetailModel))]
        public IHttpActionResult GetDetails(int id)
        {
            var article = _articleService.FindById(id);
            var comments = _commentService.FindAll(id);

            ArticleDetailModel articleModel = new ArticleDetailModel();
            articleModel = Mapper.Map<Article, ArticleDetailModel>(article);
            if (article.RankNum > 0)
                articleModel.Rank = Convert.ToDouble(Convert.ToDouble(article.RankSum) / Convert.ToDouble(article.RankNum));

            List<CommentModel> list = new List<CommentModel>();

            foreach (var item in comments)
            {
                list.Add(Mapper.Map<Comment, CommentModel>(item));
            }
            articleModel.Comments = list;

            return Ok(articleModel);
        }

        // POST
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Create(ArticleCreateModel articleModel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                DateTime date = DateTime.Now;
                Random random = new Random();
                var fileName = "img_" + random.Next() + ".png";

                var contentType = "Image/png";
                HttpPostedFileBaseCustom HttpPostedFileBaseCustom = new HttpPostedFileBaseCustom(GetSteamFromBase64String(articleModel.File), contentType, fileName);
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), HttpPostedFileBaseCustom.FileName);
                HttpPostedFileBaseCustom.SaveAs(fileSavePath);

                articleModel.Image = fileName;
                articleModel.CreateDate = DateTime.Now;
                var article = Mapper.Map<ArticleCreateModel, Article>(articleModel);
                _articleService.Create(article);


                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        // PUT 
        [Authorize]
        public HttpResponseMessage Edit(ArticleEditModel articleModel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                DateTime date = DateTime.Now;
                Random random = new Random();
                var fileName = "img_" + random.Next() + ".png";

                var contentType = "Image/png";
                HttpPostedFileBaseCustom HttpPostedFileBaseCustom = new HttpPostedFileBaseCustom(GetSteamFromBase64String(articleModel.File), contentType, fileName);
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), HttpPostedFileBaseCustom.FileName);
                HttpPostedFileBaseCustom.SaveAs(fileSavePath);

                articleModel.Image = fileName;
                var article = Mapper.Map<ArticleEditModel, Article>(articleModel);
                _articleService.Edit(article);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        // DELETE 
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                var article = _articleService.FindById(id);
                _articleService.Delete(article);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage CreateComment(CommentCreateModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                commentModel.CommentDate = DateTime.Now;
                commentModel.IsBlocked = false;
                _commentService.Create(Mapper.Map<CommentCreateModel, Comment>(commentModel));

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage EditRankArticle(ArticleRankModel articleRankModel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                // string userId = User.Identity.GetUserId();
                _articleService.RankArticle(articleRankModel.ArticleId, articleRankModel.UserId, articleRankModel.Rank);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        public async Task<HttpResponseMessage> PostFile()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body

                // Read the form data and return an async task.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        sb.Append(string.Format("{0}: {1}\n", key, val));
                    }
                }

                // This illustrates how to get the file names for uploaded files.
                foreach (var file in provider.FileData)
                {
                    FileInfo fileInfo = new FileInfo(file.LocalFileName);
                    sb.Append(string.Format("Uploaded file: {0} ({1} bytes)\n", fileInfo.Name, fileInfo.Length));
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(sb.ToString())
                };
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        public MemoryStream GetSteamFromBase64String(string imageBase64)
        {
            if (imageBase64.IndexOf(',') > 0)
            {
                imageBase64 = imageBase64.Substring(imageBase64.IndexOf(',') + 1);
            }
            byte[] bytes = Convert.FromBase64String(imageBase64);
            var ms = new MemoryStream(bytes);

            return ms;
        }
        public MemoryStream GetSteamFromByeArray(byte[] bytes)
        {
            var ms = new MemoryStream(bytes);

            return ms;
        }
    }
}