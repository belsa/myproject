﻿using MyBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBlog.DAL
{
    public class BlogInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BlogContext>
    {
        protected override void Seed(BlogContext context)
        {

            //users.ForEach(s => context.Users.Add(s));
            //context.SaveChanges();

            var articles = new List<ArticleModel>
            {
            new ArticleModel{Id=1,UserId="",Detail="NweArticlesDetail",Title="NewArticleTitle",Rank=0,Image=null,CreateDate=DateTime.Now}
            };
            articles.ForEach(s => context.Articles.Add(s));
            context.SaveChanges();

        }
    }
}
