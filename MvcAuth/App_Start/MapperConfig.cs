﻿using AutoMapper;
using MyBlog.Entities;
using MvcAuth.Models;
using MvcAuth.Models.Api;

namespace MvcAuth
{
    public static class MapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Article, ArticleCreateModel>().ReverseMap();
                cfg.CreateMap<Article, ArticleEditModel>().ReverseMap();
                cfg.CreateMap<Article, ArticleModel>().ReverseMap();
                cfg.CreateMap<Comment, CommentModel>().ReverseMap();
                cfg.CreateMap<Comment, CommentCreateModel>().ReverseMap();
                cfg.CreateMap<Article, ArticleViewModel>().ReverseMap();
                cfg.CreateMap<Comment, CommentViewModel>().ReverseMap();
                cfg.CreateMap<Report, ReportViewModel>().ReverseMap(); 
            });
        } 
    }
}