﻿using System;
using System.Data.Entity;
using System.Reflection;
using System.Web.Mvc;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Core;
using MvcAuth.Controllers;
using MyBlog.Data.Repository;
using MyBlog.Data.Service;
using MyBlog.Data.Context;
using Autofac.Integration.WebApi;
using System.Web.Http;

namespace MvcAuth
{
    public partial class Startup
    {
        public void ConfigureReg(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Register dependencies, then...
            var controllersAssembly = Assembly.GetAssembly(typeof(AccountController));
            //builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterControllers(controllersAssembly);
            builder.RegisterType(typeof(ArticleRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ArticleService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CommentRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CommentService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ReportRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ReportService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(UserRolesRepository)).AsImplementedInterfaces();
            builder.RegisterType(typeof(UserRoleService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ApplicationDbContext)).AsImplementedInterfaces();

            //builder.RegisterType<ApplicationDbContext>().AsSelf().InstancePerRequest();
            //builder.RegisterType<IApplicationDbContext>().AsSelf().InstancePerRequest();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication);
            //builder.Register<IdentityFactoryOptions<ApplicationUserManager>>(c => new IdentityFactoryOptions<ApplicationUserManager>() { DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("ApplicationName") });

            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            //builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterFilterProvider();
            var dbContextParameter = new ResolvedParameter((pi, ctx) => pi.ParameterType == typeof(DbContext),
                                                        (pi, ctx) => ctx.Resolve<ApplicationDbContext>());

            builder.RegisterType<UserStore<ApplicationUser>>().As<IUserStore<ApplicationUser>>().WithParameter(dbContextParameter).InstancePerLifetimeScope();


            builder.RegisterAssemblyTypes(typeof(MvcApplication).Assembly)
              .AsImplementedInterfaces();

            builder.RegisterModule(new AutofacWebTypesModule());

            builder.RegisterType<ApplicationDbContext>().InstancePerRequest();

            //builder.RegisterType<UserStore<ApplicationUser>>().As<IUserStore<ApplicationUser>>();

            builder.RegisterType<UserManager<ApplicationUser>>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Register the Autofac middleware FIRST. This also adds
            // Autofac-injected middleware registered with the container.
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
            app.UseAutofacMvc();
          
        }
    }
}