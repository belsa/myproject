﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyBlog.Data.Service;
using MyBlog.Entities;
using MyBlog.Data.Context;
using MyBlog.Data.Repository;
using MvcAuth.Controllers;
using MvcAuth.Models;
using MvcAuth.Tests.Fake;
using AutoMapper;

namespace MvcAuth.Tests
{
    [TestClass]
    public class UnitTestArticlesContoller
    {
        private Mock<HttpContextBase> _fakeHttpContext;
        private GenericIdentity _fakeIdentity;
        private Mock<ControllerContext> _controllerContext;

        [TestInitialize]
        public void SetupTests()
        {
            MapperConfig.RegisterMaps();

            var mockServer = new Mock<HttpServerUtilityBase>();
            mockServer.Setup(x => x.MapPath("~/images")).Returns("c:\\temp\\images\\");
            _fakeHttpContext = new Mock<HttpContextBase>();
            _fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(_fakeIdentity, null);

            _fakeHttpContext.Setup(t => t.User).Returns(principal);
            _fakeHttpContext.Setup(x => x.Server).Returns(mockServer.Object);
            _controllerContext = new Mock<ControllerContext>();
            _controllerContext.Setup(t => t.HttpContext).Returns(_fakeHttpContext.Object);
        }
        [TestMethod]
        public void ArticlesController_Index_Should_Return_Records()
        {
            // Given
            var articles = new List<Article>();
            articles.Add(FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5));
            articles.Add(FakeArticleFactory.CreateFakeArticle(2, Guid.NewGuid().ToString(), "userB", "titleB", "detailB", "bb.jpg", 2, 10));

            var repositoryComment = new Mock<IServiceComment>();
            var repositoryArticle = new Mock<IServiceArticle>();
            repositoryArticle.Setup(e => e.FindAll()).Returns(articles.AsQueryable());

            var controller = new ArticlesController(repositoryArticle.Object, repositoryComment.Object);

            //Act
            var viewResult = controller.Index() as ViewResult;
            var records = viewResult.Model as List<ArticleViewModel>;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(records);
            Assert.AreEqual(2, records.Count);
        }
        [TestMethod]
        public void ArticlesController_Details_Should_Not_Be_Null()
        {
            // Given
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5);
            var comments = new List<Comment>();
            comments.Add(FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false));
            comments.Add(FakeCommentFactory.CreateFakeComment(2, 1, Guid.NewGuid().ToString(), "userC", "commentDetailC", true));
            comments.Add(FakeCommentFactory.CreateFakeComment(3, 1, Guid.NewGuid().ToString(), "userD", "commentDetailD", false));

            var repositoryComment = new Mock<IServiceComment>();
            var repositoryArticle = new Mock<IServiceArticle>();

            repositoryArticle.Setup(e => e.FindById(1)).Returns(article);
            repositoryComment.Setup(e => e.FindAll(1)).Returns(comments.AsQueryable());

            var controller = new ArticlesController(repositoryArticle.Object, repositoryComment.Object);
            //Act
            var viewResult = controller.Details(1) as ViewResult;
            var record = viewResult.Model as ArticleCommentsViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(3, record.Comments.Count);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("titleA", record.Title);
            Assert.AreEqual("userA", record.UserName);
            Assert.AreEqual("detailA", record.Detail);
            Assert.AreEqual("aa.jpg", record.Image);
            Assert.AreEqual(1, record.RankNum);
            Assert.AreEqual(5, record.RankSum);

        }
        [TestMethod]
        public void ArticlesController_Create_Article_Should_Create_Record()
        {
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "TitleNew", "DetailNew", "aa.jpg", 2, 10);
            var articles = new List<Article>(){
                FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "xyz", "AAAAA", "bb.jpg", 1, 5)
            };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(articles);
            mockContext.Setup(x => x.Article).Returns(mockSet);
            var db = mockContext.Object;

            var repositoryArticle = new ArticleRepository(db);
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);


            var articleController = new ArticlesController(articleService, commentService);

            articleController.ControllerContext = new ControllerContext(
                               _fakeHttpContext.Object, new RouteData(), articleController);
            var file = new Mock<HttpPostedFileBase>();
            file.Setup(x => x.ContentLength).Returns(1);
            file.Setup(x => x.FileName).Returns("bb.jpg");

            Assert.AreEqual(1, articles.Count);
            articleController.Create(Mapper.Map<Article,ArticleViewModel>(article), file.Object);
            Assert.AreEqual(2, articles.Count);

            // for more test
            //mockSet.Verify(m => m.Add(It.IsAny<Article>()), Times.Once());
            //mockContext.Verify(m => m.SaveChanges(), Times.Once());
            //mockSet.Verify(m => m.Add(It.Is<Article>(arg => arg.Id == 1)));

        }
        [TestMethod]
        public void ArticlesController_Create_Article_Should_Return_View()
        {
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "TitleNew", "DetailNew", "aa.jpg", 2, 10);
            var articles = new List<Article>(){
                FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "xyz", "AAAAA", "bb.jpg", 1, 5)
            };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(articles);
            mockContext.Setup(x => x.Article).Returns(mockSet);
            var db = mockContext.Object;

            var repositoryArticle = new ArticleRepository(db);
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);

            var articleController = new ArticlesController(articleService, commentService);
            var resultView = articleController.Create() as ActionResult;
            Assert.IsNotNull(resultView);
        }
        [TestMethod]
        public void ArticlesController_Create_Comment_Should_Create_Record()
        {
            var comment = FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "CommentDetailNew", true);
            var comments = new List<Comment>(){
                FakeCommentFactory.CreateFakeComment(2, 1, Guid.NewGuid().ToString(), "userA", "CommentDetail", false)
            };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(comments);
            mockContext.Setup(x => x.Comment).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryComment = new CommentRepository(db);
            var repositoryArticle = new ArticleRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);

            var articleController = new ArticlesController(articleService, commentService);
            articleController.ControllerContext = new ControllerContext(
                             _fakeHttpContext.Object, new RouteData(), articleController);
            Assert.AreEqual(1, comments.Count);
            articleController.CreateComment(Mapper.Map<Comment,CommentViewModel>(comment));
            Assert.AreEqual(2, comments.Count);

        }
        [TestMethod]
        public void ArticlesController_Create_CommentView_Should_Return_View()
        {
            var mockContext = new Mock<IApplicationDbContext>();
            var db = mockContext.Object;
            var repositoryComment = new CommentRepository(db);
            var repositoryArticle = new ArticleRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);

            var articleController = new ArticlesController(articleService, commentService);

            var resultView = articleController.CreateCommentView(1) as PartialViewResult;
            var record = resultView.Model as CommentViewModel;
            Assert.IsNotNull(resultView);
            Assert.AreEqual(1, record.ArticleId);
        }

        [TestMethod]
        public void ArticlesController_Edit_Should_Edit_Record()
        {
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "TitleNew", "DetailNew", "aa.jpg", 2, 10);
            var articles = new List<Article>
             {
                 FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "xyz", "AAAAA", "bb.jpg", 1, 5)
             };

            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(articles);
            mockContext.Setup(x => x.Article).Returns(mockSet);
            var db = mockContext.Object;

            var repositoryArticle = new ArticleRepository(db);
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);


            var articleController = new ArticlesController(articleService, commentService);

            articleController.ControllerContext = new ControllerContext(
                               _fakeHttpContext.Object, new RouteData(), articleController);
            var file = new Mock<HttpPostedFileBase>();
            file.Setup(x => x.ContentLength).Returns(1);
            file.Setup(x => x.FileName).Returns("bb.jpg");
            articleController.Edit(Mapper.Map<Article,ArticleViewModel>(article), file.Object);
            Assert.AreEqual(1, articles.Count);
            Assert.AreEqual("TitleNew", articles[0].Title);
            Assert.AreEqual("DetailNew", articles[0].Detail);
            Assert.AreEqual(2, articles[0].RankNum);
            Assert.AreEqual(10, articles[0].RankSum);
        }

        [TestMethod]
        public void ArticlesController_Edit_Should_Not_Be_Null()
        {
            // Given
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5);

            var repositoryComment = new Mock<IServiceComment>();
            var repositoryArticle = new Mock<IServiceArticle>();

            repositoryArticle.Setup(e => e.FindById(1)).Returns(article);

            var controller = new ArticlesController(repositoryArticle.Object, repositoryComment.Object);
            //Act
            var viewResult = controller.Edit(1) as ViewResult;
            var record = viewResult.Model as ArticleViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("titleA", record.Title);
            Assert.AreEqual("userA", record.UserName);
            Assert.AreEqual("detailA", record.Detail);
            Assert.AreEqual("aa.jpg", record.Image);
            Assert.AreEqual(1, record.RankNum);
            Assert.AreEqual(5, record.RankSum);
        }

        [TestMethod]
        public void ArticlesController_DeleteConfirmed_Should_Delete_Record()
        {
            var articles = new List<Article>
             {
                 FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "usera", "xyz", "AAAAA", "aa.jpg", 1, 5),
                 FakeArticleFactory.CreateFakeArticle(2, Guid.NewGuid().ToString(), "userb", "xyz", "BBBBB", "bb.jpg", 1, 5)
             };

            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(articles);
            mockContext.Setup(x => x.Article).Returns(mockSet);
            mockContext.Setup(x => x.SaveChanges())
                .Verifiable();
            var db = mockContext.Object;

            var repositoryArticle = new ArticleRepository(db);
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);

            var articleController = new ArticlesController(articleService, commentService);

            articleController.ControllerContext = new ControllerContext(
                               _fakeHttpContext.Object, new RouteData(), articleController);

            Assert.AreEqual(2, articles.Count);
            articleController.DeleteConfirmed(1);
            Assert.AreEqual(1, articles.Count);
        }

        [TestMethod]
        public void ArticlesController_Delete_Should_Not_Be_Null()
        {
            // Given
            var article = FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5);

            var repositoryComment = new Mock<IServiceComment>();
            var repositoryArticle = new Mock<IServiceArticle>();

            repositoryArticle.Setup(e => e.FindById(1)).Returns(article);

            var controller = new ArticlesController(repositoryArticle.Object, repositoryComment.Object);
            //Act
            var viewResult = controller.Delete(1) as ViewResult;
            var record = viewResult.Model as ArticleViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("titleA", record.Title);
            Assert.AreEqual("userA", record.UserName);
            Assert.AreEqual("detailA", record.Detail);
            Assert.AreEqual("aa.jpg", record.Image);
            Assert.AreEqual(1, record.RankNum);
            Assert.AreEqual(5, record.RankSum);
        }

        [TestMethod]
        public void ArticlesController_RankArticleView_Should_Not_Be_Null()
        {
            var mockContext = new Mock<IApplicationDbContext>();
            var db = mockContext.Object;
            var repositoryComment = new CommentRepository(db);
            var repositoryArticle = new ArticleRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);

            var articleController = new ArticlesController(articleService, commentService);
            articleController.ControllerContext = new ControllerContext(
                              _fakeHttpContext.Object, new RouteData(), articleController);

            var resultView = articleController.RankArticleView(1) as PartialViewResult;
            var list = resultView.ViewData["RankList"];
            Assert.IsNotNull(resultView);
            Assert.AreEqual(1, resultView.ViewData["Id"]);
            Assert.IsNotNull(list);
        }

        [TestMethod]
        public void ArticlesController_RankArticle_Should_Update_Rank_Of_Record()
        {
            var articles = new List<Article>
             {
                 FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userabc", "xyz", "AAAAA", "bb.jpg", 1, 5)
             };
            var userRanks = new List<UserRanks>
             {
                 FakeUserRanksFactory.CreateFakeUserRanks(1,1,"abc",3)
             };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSetArticle = FakeClass.GetQueryableMockDbSet(articles);
            var mockSetUserRanks = FakeClass.GetQueryableMockDbSet(userRanks);
            mockContext.Setup(x => x.Article).Returns(mockSetArticle);
            mockContext.Setup(x => x.UserRanks).Returns(mockSetUserRanks);

            var db = mockContext.Object;

            var repositoryArticle = new ArticleRepository(db);
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);
            var articleService = new ArticleService(repositoryArticle);


            var articleController = new ArticlesController(articleService, commentService);

            articleController.ControllerContext = new ControllerContext(
                               _fakeHttpContext.Object, new RouteData(), articleController);

            articleController.RankArticle(5, 1, "abc");
            articleController.RankArticle(3, 1, "xyz");

            Assert.AreEqual(2, articles[0].RankNum);
            Assert.AreEqual(8, articles[0].RankSum);
            Assert.AreEqual(2, userRanks.Count);
            Assert.AreEqual(5, userRanks[0].Rank);
        }

        [TestCleanup]
        public void TestsDown()
        {
            Mapper.Reset();
        } 
    } 
}
