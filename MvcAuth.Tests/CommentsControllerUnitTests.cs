﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyBlog.Data.Service;
using MyBlog.Entities;
using MyBlog.Data.Context;
using MyBlog.Data.Repository;
using MvcAuth.Controllers;
using MvcAuth.Models;
using MvcAuth.Tests.Fake;
using AutoMapper;

namespace MvcAuth.Tests
{
    [TestClass]
    public class CommentsControllerUnitTests
    {
        [TestInitialize]
        public void SetupTests()
        {
            MapperConfig.RegisterMaps();
        }

        [TestMethod]
        public void CommentsController_Index_Should_Return_Records()
        {
            // Given
            var comments = new List<Comment>();
            comments.Add(FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false));
            comments.Add(FakeCommentFactory.CreateFakeComment(2, 1, Guid.NewGuid().ToString(), "userC", "commentDetailC", true));
            comments.Add(FakeCommentFactory.CreateFakeComment(3, 1, Guid.NewGuid().ToString(), "userD", "commentDetailD", false));

            var repositoryComment = new Mock<IServiceComment>();

            repositoryComment.Setup(e => e.FindAll()).Returns(comments.AsQueryable());

            var controller = new CommentsController(repositoryComment.Object);

            //Act
            var viewResult = controller.Index() as ViewResult;
            var records = viewResult.Model as List<CommentViewModel>;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(records);
            Assert.AreEqual(3, records.Count);
        }
        [TestMethod]
        public void CommentsController_Details_Should_Not_Be_Null()
        {
            // Given
            var comment = FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false);
            var repositoryComment = new Mock<IServiceComment>();
            repositoryComment.Setup(e => e.FindById(1)).Returns(comment);
            var controller = new CommentsController(repositoryComment.Object);
            //Act
            var viewResult = controller.Details(1) as ViewResult;
            var record = viewResult.Model as CommentViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("userB", record.UserName);
            Assert.AreEqual("commentDetailB", record.CommentDetail);
            Assert.AreEqual(false, record.IsBlocked);

        }
        [TestMethod]
        public void CommentsController_Edit_Should_Edit_Record()
        {
            var comment = FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "xyz", true);
            var comments = new List<Comment>
             {
                 FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false)
             };

            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(comments);
            mockContext.Setup(x => x.Comment).Returns(mockSet);
            var db = mockContext.Object;

            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);

            var commentsController = new CommentsController(commentService);

            commentsController.Edit(Mapper.Map<Comment,CommentViewModel>(comment));
            Assert.AreEqual(1, comments.Count);
            Assert.AreEqual("xyz", comments[0].CommentDetail);
            Assert.AreEqual(true, comments[0].IsBlocked);


        }
        [TestMethod]
        public void CommentsController_Edit_Should_Not_Be_Null()
        {
            // Given
            var comment = FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false);
            var repositoryComment = new Mock<IServiceComment>();
            repositoryComment.Setup(e => e.FindById(1)).Returns(comment);
            var controller = new CommentsController(repositoryComment.Object);
            //Act
            var viewResult = controller.Edit(1) as ViewResult;
            var record = viewResult.Model as CommentViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("userB", record.UserName);
            Assert.AreEqual("commentDetailB", record.CommentDetail);
            Assert.AreEqual(false, record.IsBlocked);

        }
        [TestMethod]
        public void CommentsController_DeleteConfirmed_Should_Delete_Record()
        {
            var comments = new List<Comment>
             {
                 FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userA", "commentDetailA", false),
                 FakeCommentFactory.CreateFakeComment(2, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false)
             };

            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(comments);
            mockContext.Setup(x => x.Comment).Returns(mockSet);
            mockContext.Setup(x => x.SaveChanges())
                .Verifiable();
            var db = mockContext.Object;
            var repositoryComment = new CommentRepository(db);
            var commentService = new CommentService(repositoryComment);

            var commentsController = new CommentsController(commentService);
            Assert.AreEqual(2, comments.Count);
            commentsController.DeleteConfirmed(1);
            Assert.AreEqual(1, comments.Count);
        }
        [TestMethod]
        public void CommentsController_Delete_Should_Not_Be_Null()
        {
            // Given
            var comment = FakeCommentFactory.CreateFakeComment(1, 1, Guid.NewGuid().ToString(), "userB", "commentDetailB", false);
            var repositoryComment = new Mock<IServiceComment>();
            repositoryComment.Setup(e => e.FindById(1)).Returns(comment);
            var controller = new CommentsController(repositoryComment.Object);
            //Act
            var viewResult = controller.Delete(1) as ViewResult;
            var record = viewResult.Model as CommentViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual("userB", record.UserName);
            Assert.AreEqual("commentDetailB", record.CommentDetail);
            Assert.AreEqual(false, record.IsBlocked);

        }

        [TestCleanup]
        public void TestsDown()
        {
            Mapper.Reset();
        }
    }
}
