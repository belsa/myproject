﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyBlog.Data.Service;
using MyBlog.Entities;
using MvcAuth.Controllers;
using MvcAuth.Models;
using MvcAuth.Tests.Fake;
using AutoMapper;

namespace MvcAuth.Tests
{
    [TestClass]
    public class UnitTestHomeContoller
    {

        private Mock<IServiceArticle> _articleRepository;

        [TestInitialize]
        public void SetupTests()
        {
            MapperConfig.RegisterMaps();
            _articleRepository = new Mock<IServiceArticle>();
        }
        [TestMethod]
        public void HomeController_Index_Should_Return_Records_OrderByDateDesc_If_QueryString_Is_Null()
        {
            // Given
            var articles = new List<Article>();
            articles.Add(FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5, DateTime.Now));
            articles.Add(FakeArticleFactory.CreateFakeArticle(2, Guid.NewGuid().ToString(), "userB", "titleB", "detailB", "bb.jpg", 2, 10, DateTime.Now.AddMinutes(1)));

            _articleRepository.Setup(e => e.FindAllOrderByDate()).Returns(articles.AsQueryable());

            var controller = new HomeController(_articleRepository.Object);

            //Act
            var viewResult = controller.Index(null) as ViewResult;
            var records = viewResult.Model as List<ArticleViewModel>;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(records);
            Assert.AreEqual(2, records.Count);
            // Assert.IsTrue(records[0].CreateDate > records[1].CreateDate);

        }
        [TestMethod]
        public void HomeController_Index_Should_Return_Records_BySearchQuery_If_QueryString_Is_Not_Null()
        {
            // Given
            var articles = new List<Article>();
            articles.Add(FakeArticleFactory.CreateFakeArticle(1, Guid.NewGuid().ToString(), "userA", "titleA", "detailA", "aa.jpg", 1, 5));
            articles.Add(FakeArticleFactory.CreateFakeArticle(2, Guid.NewGuid().ToString(), "userB", "titleB", "detailB", "bb.jpg", 2, 10));

            _articleRepository.Setup(e => e.FindAllBySearchQuery("titleA")).Returns(articles.AsQueryable());

            var controller = new HomeController(_articleRepository.Object);

            //Act
            var viewResult = controller.Index("titleA") as ViewResult;
            var records = viewResult.Model as List<ArticleViewModel>;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(records);
            Assert.AreEqual(2, records.Count);
        }
        [TestMethod]
        public void HomeController_Details_Should_RedirectsToArticleDetails()
        {
            // Arrange
            var controller = new HomeController(_articleRepository.Object);

            // Act
            var result = (RedirectToRouteResult)controller.Details(1);

            // Assert
            Assert.AreEqual("Details", result.RouteValues["action"]);
            Assert.AreEqual("Articles", result.RouteValues["controller"]);
            Assert.AreEqual(1, result.RouteValues["Id"]);

        }
        [TestMethod]
        public void HomeController_About_Should_Not_Be_Null()
        {
            // Arrange
            var controller = new HomeController(_articleRepository.Object);
            // Act
            var viewResult = controller.About() as ViewResult;
            // Assert
            Assert.IsNotNull(viewResult);
        }
        [TestMethod]
        public void HomeController_Contact_Should_Not_Be_Null()
        {
            // Arrange
            var controller = new HomeController(_articleRepository.Object);
            // Act
            var viewResult = controller.Contact() as ViewResult;
            // Assert
            Assert.IsNotNull(viewResult);
        }

        [TestCleanup]
        public void TestsDown()
        {
            Mapper.Reset();
        }
    }
}
