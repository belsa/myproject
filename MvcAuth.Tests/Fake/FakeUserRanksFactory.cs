﻿using MyBlog.Entities;
using System;

namespace MvcAuth.Tests.Fake
{
    public static class FakeUserRanksFactory
    {
        public static UserRanks CreateFakeUserRanks(int Id,int articleId, string userId,int rank)
        {
            return new UserRanks { Id = Id, ArticleId = articleId, UserId = userId,Rank =rank };
        }
    }
}
