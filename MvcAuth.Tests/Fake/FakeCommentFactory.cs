﻿using MyBlog.Entities;
using System;

namespace MvcAuth.Tests.Fake
{
    public static class FakeCommentFactory
    {
        public static Comment CreateFakeComment(int Id,int articleId, string userId,string userName,string commentDetail,bool isBlocked)
        {
            return new Comment { Id = Id, ArticleId = articleId, UserId = userId, UserName = userName, CommentDate = DateTime.Now, CommentDetail = commentDetail, IsBlocked = isBlocked };
        }
    }
}
