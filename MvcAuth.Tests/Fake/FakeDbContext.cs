﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using FakeDbSet;
using MyBlog.Data.Context;
using MyBlog.Entities;

namespace MvcAuth.Tests.Fake
{
    public class FakeDbContext : ApplicationDbContext , IDisposable
    {
        /// <summary>
        /// Sets up the fake database.
        /// </summary>
        public FakeDbContext()
        {
            // We're setting our DbSets to be InMemoryDbSets rather than using SQL Server.
            //Article = new InMemoryDbSet<Article> { FindFunction = (a, i) => a.FirstOrDefault(x => x.Id == i.Cast<int>().First()) };
            //Comment = new InMemoryDbSet<Comment> { FindFunction = (a, i) => a.FirstOrDefault(x => x.Id == i.Cast<int>().First()) };
            //Report = new InMemoryDbSet<Report> { FindFunction = (a, i) => a.FirstOrDefault(x => x.Id == i.Cast<int>().First()) };

        }

        public IDbSet<Article> Article { get; set; }
        public IDbSet<Comment> Comment { get; set; }
        public IDbSet<Report> Report { get; set; }

        public int SaveChanges()
        {
            // Pretend that each entity gets a database id when we hit save.
            int changes = 0;
            changes += DbSetHelper.IncrementPrimaryKey(x => x.Id, Article);
            changes += DbSetHelper.IncrementPrimaryKey(x => x.Id, Comment);
            changes += DbSetHelper.IncrementPrimaryKey(x => x.Id, Report);

            return changes;
        }

        public  void Dispose()
        {
            // Do nothing!
        }
    }

}
