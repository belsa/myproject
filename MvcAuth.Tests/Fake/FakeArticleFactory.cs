﻿using MyBlog.Entities;
using MvcAuth.Models;
using System;

namespace MvcAuth.Tests.Fake
{
    public static class FakeArticleFactory
    {
        public static Article CreateFakeArticle(int Id, string userId,string userName,string title,string detail,string image,int rankNum,int rankSum)
        {
            return new Article { Id = Id, UserId = userId, UserName = userName, Title = title, Detail = detail, Image = image, CreateDate = DateTime.Now, RankNum = rankNum, RankSum = rankSum };
        }
        public static Article CreateFakeArticle(int Id, string userId, string userName, string title, string detail, string image, int rankNum, int rankSum, DateTime createDate)
        {
            return new Article { Id = Id, UserId = userId, UserName = userName, Title = title, Detail = detail, Image = image, CreateDate = createDate, RankNum = rankNum, RankSum = rankSum };
        }
        public static ArticleViewModel CreateFakeArticleViewModel(int Id, string userId, string userName, string title, string detail, string image, int rankNum, int rankSum)
        {
            return new ArticleViewModel { Id = Id, UserId = userId, UserName = userName, Title = title, Detail = detail, Image = image, CreateDate = DateTime.Now, RankNum = rankNum, RankSum = rankSum };
        }
        
    }
}
