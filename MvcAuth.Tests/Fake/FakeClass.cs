﻿using MyBlog.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBlog.Entities;
using System.Data.Entity;
using Moq;

namespace MvcAuth.Tests.Fake
{
    public class FakeClass
    {
        private IApplicationDbContext _dbContext;
        public FakeClass(IApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Edit(Article article)
        {
            var entity = _dbContext.Article.Where(x => x.Id == article.Id).Single();

            entity.Title = article.Title;
            entity.Detail = article.Detail;
            _dbContext.SaveChanges();

        }

        public static IDbSet<T> GetQueryableMockDbSet<T>(List<T> sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<IDbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => sourceList.Add(s));
            dbSet.Setup(d => d.Remove(It.IsAny<T>())).Callback<T>((s) => sourceList.Remove(s));

            return dbSet.Object;
        }

    }
}
