﻿using MyBlog.Entities;
using System;

namespace MvcAuth.Tests.Fake
{
    public static class FakeReportFactory
    {
        public static Report CreateFakeReport(int Id,int commentId, string userId,string userName,string reportDetail)
        {
            return new Report { Id = Id,CommentId=commentId, UserId = userId,UserName=userName,ReportDate=DateTime.Now,ReportDetail=reportDetail};
        }
    }
}
