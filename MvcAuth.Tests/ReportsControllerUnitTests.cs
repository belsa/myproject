﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyBlog.Data.Service;
using MyBlog.Entities;
using MyBlog.Data.Context;
using MyBlog.Data.Repository;
using MvcAuth.Controllers;
using MvcAuth.Models;
using MvcAuth.Tests.Fake;
using System.Web.Routing;
using AutoMapper;

namespace MvcAuth.Tests
{
    [TestClass]
    public class ReportsControllerUnitTests
    {
        private Mock<HttpContextBase> _fakeHttpContext;
        private GenericIdentity _fakeIdentity;
        private Mock<ControllerContext> _controllerContext;
      

        [TestInitialize]
        public void SetupTests()
        {
            MapperConfig.RegisterMaps();
            var mockServer = new Mock<HttpServerUtilityBase>();
            mockServer.Setup(x => x.MapPath("~/images")).Returns("c:\\temp\\images\\");
            _fakeHttpContext = new Mock<HttpContextBase>();
            _fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(_fakeIdentity, null);

            _fakeHttpContext.Setup(t => t.User).Returns(principal);
            _fakeHttpContext.Setup(x => x.Server).Returns(mockServer.Object);
            _controllerContext = new Mock<ControllerContext>();
            _controllerContext.Setup(t => t.HttpContext).Returns(_fakeHttpContext.Object);
        }
        [TestMethod]
        public void ReportsController_Index_Should_Return_Records()
        {
            // Given
            var reports = new List<Report>();
            FakeReportFactory.CreateFakeReport(1, 1, "", "", "");
            reports.Add(FakeReportFactory.CreateFakeReport(1, 1, Guid.NewGuid().ToString(), "userA", "ReportDetailA"));
            reports.Add(FakeReportFactory.CreateFakeReport(1, 1, Guid.NewGuid().ToString(), "userB", "ReportDetailB"));

            var repositoryReport = new Mock<IServiceReport>();

            repositoryReport.Setup(e => e.FindAll()).Returns(reports.AsQueryable());

            var controller = new ReportsController(repositoryReport.Object);

            //Act
            var viewResult = controller.Index() as ViewResult;
            var records = viewResult.Model as List<ReportViewModel>;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.IsNotNull(records);
            Assert.AreEqual(2, records.Count);
        }
        [TestMethod]
        public void ReportsController_Details_Should_Not_Be_Null()
        {
            // Given
            var report = FakeReportFactory.CreateFakeReport(1, 1, Guid.NewGuid().ToString(), "userA", "ReportDetailA");
            var repositoryReport = new Mock<IServiceReport>();
            repositoryReport.Setup(e => e.FindById(1)).Returns(report);
            var controller = new ReportsController(repositoryReport.Object);
            //Act
            var viewResult = controller.Details(1) as ViewResult;
            var record = viewResult.Model as ReportViewModel;
            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(1, record.Id);
            Assert.AreEqual(1, record.CommentId);
            Assert.AreEqual("userA", record.UserName);
            Assert.AreEqual("ReportDetailA", record.ReportDetail);

        }
        [TestMethod]
        public void ReportsController_Create_Should_Create_Record()
        {
            var report = FakeReportFactory.CreateFakeReport(2, 1, Guid.NewGuid().ToString(), "userB", "ReportDetailB");
            var reports = new List<Report>(){
               FakeReportFactory.CreateFakeReport(1, 1, Guid.NewGuid().ToString(), "userA", "ReportDetailA")
        };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(reports);
            mockContext.Setup(x => x.Report).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryReport = new ReportRepository(db);
            var reportService = new ReportService(repositoryReport);

            var reportsController = new ReportsController(reportService);
            reportsController.ControllerContext = new ControllerContext(
                             _fakeHttpContext.Object, new RouteData(), reportsController);

            Assert.AreEqual(1, reports.Count);
            reportsController.Create(Mapper.Map<Report,ReportViewModel>(report),1);
            Assert.AreEqual(2, reports.Count);
        }
        [TestMethod]
        public void ReportsController_Create_Should_Return_View()
        {
            var report = FakeReportFactory.CreateFakeReport(2, 1, Guid.NewGuid().ToString(), "userB", "ReportDetailB");
            var reports = new List<Report>(){
               FakeReportFactory.CreateFakeReport(1, 1, Guid.NewGuid().ToString(), "userA", "ReportDetailA")
        };
            var mockContext = new Mock<IApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(reports);
            mockContext.Setup(x => x.Report).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryReport = new ReportRepository(db);
            var reportService = new ReportService(repositoryReport);

            var reportsController = new ReportsController(reportService);

            var resultView = reportsController.Create(1) as ViewResult;
            Assert.IsNotNull(resultView);
            Assert.AreEqual(1, resultView.ViewData["Id"]);

        }

        [TestCleanup]
        public void TestsDown()
        {
            Mapper.Reset();
        }
    }
}
