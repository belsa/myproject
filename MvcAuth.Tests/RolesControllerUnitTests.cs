﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using MyBlog.Data.Service;
using MyBlog.Entities;
using MyBlog.Data.Context;
using MyBlog.Data.Repository;
using MvcAuth.Controllers;
using MvcAuth.Models;
using MvcAuth.Tests.Fake;
using AutoMapper;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace MvcAuth.Tests
{
    [TestClass]
    public class RolesControllerUnitTests
    {
        private Mock<HttpContextBase> _fakeHttpContext;
        private GenericIdentity _fakeIdentity;
        private Mock<ControllerContext> _controllerContext;

        [TestInitialize]
        public void SetupTests()
        {

            var mockServer = new Mock<HttpServerUtilityBase>();
            mockServer.Setup(x => x.MapPath("~/images")).Returns("c:\\temp\\images\\");
            _fakeHttpContext = new Mock<HttpContextBase>();
            _fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(_fakeIdentity, null);

            _fakeHttpContext.Setup(t => t.User).Returns(principal);
            _fakeHttpContext.Setup(x => x.Server).Returns(mockServer.Object);
            _controllerContext = new Mock<ControllerContext>();
            _controllerContext.Setup(t => t.HttpContext).Returns(_fakeHttpContext.Object);
            
        }

        [TestMethod]
        public void RolesController_Index_Should_Return_Records()
        {
            // Given
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
                new IdentityRole() {Name="User"}

            };
            var users = new List<IdentityUser>()
            {
               new IdentityUser() {UserName="bb@bb"},
                new IdentityUser() {UserName="ss@ss"}

            };

            var repositoryUserRoles = new Mock<IServiceUserRoles>();
            repositoryUserRoles.Setup(e => e.FindAll()).Returns(roles.ToList().Select(rr =>
            new SelectListItem { Value = rr.Id.ToString(), Text = rr.Name }).ToList());
            repositoryUserRoles.Setup(e => e.FindAllUsers()).Returns(users.ToList().Select(uu =>
            new SelectListItem { Value = uu.Id.ToString(), Text = uu.UserName }).ToList());

            var controller = new RolesController(repositoryUserRoles.Object);

            //Act
            var viewResult = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(viewResult);
            Assert.AreEqual(2, viewResult.ViewBag.Users.Count);
            Assert.AreEqual(2, viewResult.ViewBag.Roles.Count);

        }
        [TestMethod]
        public void RolesController_Create_Should_Return_View()
        {

            var repositoryUserRoles = new Mock<IServiceUserRoles>();
            var controller = new RolesController(repositoryUserRoles.Object);

            //Act
            var viewResult = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(viewResult);


        }
        [TestMethod]
        public void RolesController_Create_Should_Create_Record()
        {
            var role = new IdentityRole { Name = "User" };
            // Given
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"}

            };
            var mockContext = new Mock<ApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(roles);
            mockContext.Setup(x => x.Roles).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);

            var formCollection = new FormCollection();
            formCollection.Add("RoleName", "User");
            rolesController.Create(formCollection);
            Assert.AreEqual(2, roles.Count);

        }
        [TestMethod]
        public void RolesController_Delete_Should_Delete_Record()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var mockContext = new Mock<ApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(roles);
            mockContext.Setup(x => x.Roles).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            string roleId = roles[0].Id;
            rolesController.Delete(roleId);
            Assert.AreEqual(1, roles.Count);
        }
        [TestMethod]
        public void RolesController_Edit_Should_Not_Be_Null()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var mockContext = new Mock<ApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(roles);
            mockContext.Setup(x => x.Roles).Returns(mockSet);
            var db = mockContext.Object;
          
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            string roleId = roles[0].Id;
            var viewResult= rolesController.Edit(roleId);
            Assert.IsNotNull(viewResult);
        }
        [TestMethod]
        public void RolesController_Edit_Should_Edit_Record()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var mockContext = new Mock<ApplicationDbContext>();
            var mockSet = FakeClass.GetQueryableMockDbSet(roles);
            mockContext.Setup(x => x.Roles).Returns(mockSet);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            IdentityRole role = new IdentityRole();
            role.Id = roles[0].Id;
            role.Name = "Admin";
            rolesController.Edit(role);
            Assert.IsNotNull("Admin",roles[0].Name);
        }
        [TestMethod]
        public void RolesController_RoleAddToUser_Should_Add_Record()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var users = new List<ApplicationUser>()
            {
               new ApplicationUser() {UserName="bb@bb"},
                new ApplicationUser() {UserName="ss@ss"}

            };
            var userRoles = new List<IdentityUserRole>()
            {
               new IdentityUserRole() {UserId= users[0].Id,RoleId=roles[0].Id}

            };
            var mockContext = new Mock<ApplicationDbContext>();
            var mockSetRoles = FakeClass.GetQueryableMockDbSet(roles);
            var mockSetUsers = FakeClass.GetQueryableMockDbSet(users);
            var mockSetUsersRoles = FakeClass.GetQueryableMockDbSet(userRoles);
            mockContext.Setup(x => x.Roles).Returns(mockSetRoles);
            mockContext.Setup(x => x.Users).Returns(mockSetUsers);
            mockContext.Setup(x => x.UserRoles).Returns(mockSetUsersRoles);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            string roleId = roles[1].Id;
            string userId = users[1].Id;
            rolesController.ControllerContext = new ControllerContext(
                            _fakeHttpContext.Object, new RouteData(), rolesController);
            rolesController.RoleAddToUser(userId,roleId);
            //Assert.AreEqual(2, userRoles.Count);
            mockContext.Verify();
        }
        [TestMethod]
        public void RolesController_GetRoles_Should_Retuns_Records()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var users = new List<ApplicationUser>()
            {
               new ApplicationUser() {UserName="bb@bb"},
               new ApplicationUser() {UserName="ss@ss"}

            };
            var userRoles = new List<IdentityUserRole>()
            {
               new IdentityUserRole() {UserId= users[0].Id,RoleId=roles[0].Id},
               new IdentityUserRole() {UserId= users[1].Id,RoleId=roles[1].Id}

            };

            var mockContext = new Mock<ApplicationDbContext>();
            var mockSetRoles = FakeClass.GetQueryableMockDbSet(roles);
            var mockSetUsers = FakeClass.GetQueryableMockDbSet(users);
            var mockSetUserRoles = FakeClass.GetQueryableMockDbSet(userRoles);
            mockContext.Setup(x => x.Roles).Returns(mockSetRoles);
            mockContext.Setup(x => x.Users).Returns(mockSetUsers);
            mockContext.Setup(x => x.UserRoles).Returns(mockSetUserRoles);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            string userId = users[0].Id;
            rolesController.ControllerContext = new ControllerContext(
                            _fakeHttpContext.Object, new RouteData(), rolesController);
            var viewResult = rolesController.GetRoles(userId) as ActionResult;
            Assert.IsNotNull(viewResult);
           
        }
        [TestMethod]
        public void RolesController_DeleteRoleForUser_Should_Delete_Record()
        {
            var roles = new List<IdentityRole>()
            {
               new IdentityRole() {Name="Administrator"},
               new IdentityRole() {Name="User"}

            };
            var users = new List<ApplicationUser>()
            {
               new ApplicationUser() {UserName="bb@bb"},
               new ApplicationUser() {UserName="ss@ss"}

            };
            var userRoles = new List<IdentityUserRole>()
            {
               new IdentityUserRole() {UserId= users[0].Id,RoleId=roles[0].Id},
               new IdentityUserRole() {UserId= users[1].Id,RoleId=roles[1].Id}

            };

            var mockContext = new Mock<ApplicationDbContext>();
            var mockSetRoles = FakeClass.GetQueryableMockDbSet(roles);
            var mockSetUsers = FakeClass.GetQueryableMockDbSet(users);
            var mockSetUserRoles = FakeClass.GetQueryableMockDbSet(userRoles);
            mockContext.Setup(x => x.Roles).Returns(mockSetRoles);
            mockContext.Setup(x => x.Users).Returns(mockSetUsers);
            mockContext.Setup(x => x.UserRoles).Returns(mockSetUserRoles);
            var db = mockContext.Object;
            var repositoryRoles = new UserRolesRepository(db);
            var rolesService = new UserRoleService(repositoryRoles);

            var rolesController = new RolesController(rolesService);
            string roleId = roles[0].Id;
            string userId = users[0].Id;
            rolesController.ControllerContext = new ControllerContext(
                            _fakeHttpContext.Object, new RouteData(), rolesController);
            rolesController.DeleteRoleForUser(userId, roleId);
            //Assert.AreEqual(1, userRoles.Count);
            mockContext.Verify();
            // var userRoles = userManager.GetRoles(user.Id);
        }
    }
}
